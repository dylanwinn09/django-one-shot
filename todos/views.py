from django.shortcuts import render
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
from django.shortcuts import render, get_object_or_404, redirect


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "all_lists": lists,
    }

    return render(request, "todos/main.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)

    context = {"todo_object": list}

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "todo_object": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "todo_item_object": item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
